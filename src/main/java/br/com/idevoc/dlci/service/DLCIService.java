package br.com.idevoc.dlci.service;

import br.com.idevoc.dlci.model.Characters;
import br.com.idevoc.dlci.model.dto.CharactersRequestDTO;
import br.com.idevoc.dlci.model.dto.CharactersResponseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

@Service
@Slf4j
public class DLCIService {

    StringBuilder outputCi = new StringBuilder();
    StringBuilder diKeyCi = new StringBuilder();
    StringBuilder outputDi = new StringBuilder();

    public CharactersResponseDTO Ciser(Characters characters) throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {

        // DLCI
        String cOrigin = characters.getSequence().toLowerCase();

        for (int i = 0; i < cOrigin.length(); i++) {
            perAdd(String.valueOf(cOrigin.charAt(i)));
        }

        // RSA
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        // ENCRYPTION
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

        byte[] cipherTextBytes = cipher.doFinal(outputCi.toString().getBytes(StandardCharsets.UTF_8));

        // CONVERSION of raw bytes to BASE64 representation
        String cipherText = Base64.getEncoder().encodeToString(cipherTextBytes);

        String publicKeyString = Base64.getEncoder().encodeToString(publicKey.getEncoded());
        String privateKeyString = Base64.getEncoder().encodeToString(privateKey.getEncoded());

        return CharactersResponseDTO.builder()
                .encryptedString(cipherText)
                .diKey(diKeyCi.toString())
                .publicKey(publicKeyString)
                .privateKey(privateKeyString)
                .build();
    }

    public Characters Diser(CharactersRequestDTO characters) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        String inputCi = characters.getEncryptedString();
        String dikeyDi = characters.getDiKey();

        // RSA
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(characters.getPrivateKey()));
        PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);

        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privKey);

        byte[] decryptedCipherTextBytes = cipher.doFinal(Base64.getDecoder().decode(inputCi));
        String decryptedCipherText = new String(decryptedCipherTextBytes,StandardCharsets.UTF_8).toLowerCase();

        // DLCI
        for (int i = 0; i < decryptedCipherText.length(); i++) {
            char charKey = dikeyDi.charAt(i);
            posAdd(String.valueOf(decryptedCipherText.charAt(i)), String.valueOf(charKey));
        }

        return Characters.builder().sequence(outputDi.toString()).build();
    }

    public void perAdd(String value) {
        switch (value) {
            case "a" -> {
                valAdd("2", "1");
            }
            case "b" -> {
                valAdd("2", "2");
            }
            case "c" -> {
                valAdd("2", "3");
            }
            case "d" -> {
                valAdd("3", "1");
            }
            case "e" -> {
                valAdd("3", "2");
            }
            case "f" -> {
                valAdd("3", "3");
            }
            case "g" -> {
                valAdd("4", "1");
            }
            case "h" -> {
                valAdd("4", "2");
            }
            case "i" -> {
                valAdd("4", "3");
            }
            case "j" -> {
                valAdd("5", "1");
            }
            case "k" -> {
                valAdd("5", "2");
            }
            case "l" -> {
                valAdd("5", "3");
            }
            case "m" -> {
                valAdd("6", "1");
            }
            case "n" -> {
                valAdd("6", "2");
            }
            case "o" -> {
                valAdd("6", "3");
            }
            case "p" -> {
                valAdd("7", "1");
            }
            case "q" -> {
                valAdd("7", "2");
            }
            case "r" -> {
                valAdd("7", "3");
            }
            case "s" -> {
                valAdd("7", "4");
            }
            case "t" -> {
                valAdd("8", "1");
            }
            case "u" -> {
                valAdd("8", "2");
            }
            case "v" -> {
                valAdd("8", "3");
            }
            case "w" -> {
                valAdd("9", "1");
            }
            case "x" -> {
                valAdd("9", "2");
            }
            case "y" -> {
                valAdd("9", "3");
            }
            case "z" -> {
                valAdd("9", "4");
            }
            case " " -> {
                valAdd("#", "!");
            }
            default -> {
                log.error("Not found char!");
            }
        }

    }

    public void posAdd(String c, String k) {
        switch (c) {
            case "#":
                if ("!".equals(k)) {
                    outputDi.append(" ");
                }
                break;

            case "2":
                if ("1".equals(k)){
                    outputDi.append("a");
                } else if ("2".equals(k)) {
                    outputDi.append("b");
                } else {
                    outputDi.append("c");
                }
                break;

            case "3":
                if ("1".equals(k)){
                    outputDi.append("d");
                } else if ("2".equals(k)) {
                    outputDi.append("e");
                } else {
                    outputDi.append("f");
                }
                break;

            case "4":
                if ("1".equals(k)){
                    outputDi.append("g");
                } else if ("2".equals(k)) {
                    outputDi.append("h");
                } else {
                    outputDi.append("i");
                }
                break;

            case "5":
                if ("1".equals(k)){
                    outputDi.append("j");
                } else if ("2".equals(k)) {
                    outputDi.append("k");
                } else {
                    outputDi.append("l");
                }
                break;

            case "6":
                if ("1".equals(k)){
                    outputDi.append("m");
                } else if ("2".equals(k)) {
                    outputDi.append("n");
                } else {
                    outputDi.append("o");
                }
                break;

            case "7":
                if ("1".equals(k)){
                    outputDi.append("p");
                } else if ("2".equals(k)) {
                    outputDi.append("q");
                } else if ("3".equals(k)) {
                    outputDi.append("r");
                } else {
                    outputDi.append("s");
                }
                break;

            case "8":
                if ("1".equals(k)){
                    outputDi.append("t");
                } else if ("2".equals(k)) {
                    outputDi.append("u");
                } else {
                    outputDi.append("v");
                }
                break;

            case "9":
                if ("1".equals(k)){
                    outputDi.append("w");
                } else if ("2".equals(k)) {
                    outputDi.append("x");
                } else if ("3".equals(k)) {
                    outputDi.append("y");
                } else {
                    outputDi.append("z");
                }
                break;

            default:
                log.error("Not found char!");
        }
    }

    public void valAdd(String c, String k) {
        outputCi.append(c);
        diKeyCi.append(k);
    }

    public String cipher(String characters) throws NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException {

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(4096);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        // ENCRYPTION
        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

        byte[] cipherTextBytes = cipher.doFinal(characters.getBytes(StandardCharsets.UTF_8));

        // CONVERSION of raw bytes to BASE64 representation
        String cipherText = Base64.getEncoder().encodeToString(cipherTextBytes);

        return cipherText;
    }

}

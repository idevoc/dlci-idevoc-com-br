package br.com.idevoc.dlci.controller;

import br.com.idevoc.dlci.model.Characters;
import br.com.idevoc.dlci.model.dto.CharactersRequestDTO;
import br.com.idevoc.dlci.model.dto.CharactersResponseDTO;
import br.com.idevoc.dlci.service.DLCIService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@RestController
@RequestMapping("/api/dlci")
@CrossOrigin("*")
public class DLCIController {

    @Autowired
    private DLCIService service;

    @ApiOperation(value = "Realiza a criptografia proprietária DLCI e aplica a RSA Assimétrica")
    @PostMapping("/ci")
    @ResponseStatus(HttpStatus.CREATED)
    public CharactersResponseDTO ci(@RequestBody Characters characters) throws NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, NoSuchPaddingException, IllegalBlockSizeException {
        return service.Ciser(characters);
    }

    @ApiOperation(value = "Realiza o processo de descriptografia")
    @PostMapping("/di")
    @ResponseStatus(HttpStatus.OK)
    public Characters ci(@RequestBody CharactersRequestDTO characters) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        return service.Diser(characters);
    }

}

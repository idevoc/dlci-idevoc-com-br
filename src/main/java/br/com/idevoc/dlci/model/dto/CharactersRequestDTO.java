package br.com.idevoc.dlci.model.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class CharactersRequestDTO {

    private String encryptedString;
    private String diKey;
    private String publicKey;
    private String privateKey;

}

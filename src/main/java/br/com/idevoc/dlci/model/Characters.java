package br.com.idevoc.dlci.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Characters {

    private String sequence;

}

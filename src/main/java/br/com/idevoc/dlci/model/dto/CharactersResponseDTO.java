package br.com.idevoc.dlci.model.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class CharactersResponseDTO {

    private String encryptedString;
    private String diKey;
    private String publicKey;
    private String privateKey;

}

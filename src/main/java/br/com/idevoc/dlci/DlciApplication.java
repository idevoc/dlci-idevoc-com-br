package br.com.idevoc.dlci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DlciApplication {

	public static void main(String[] args) {
		SpringApplication.run(DlciApplication.class, args);
	}

}

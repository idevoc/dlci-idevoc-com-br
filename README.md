Java Spring template project
This project is based on a GitLab Project Template.
Improvements can be proposed in the original project.

CI/CD with Auto DevOps
This template is compatible with Auto DevOps.
If Auto DevOps is not already enabled for this project, you can turn it on in the project settings.
